# Changelog
Note: version releases in the 0.x.y range may introduce breaking changes.

## 0.2.1

- patch: Fixed date and time format in the build duration.

## 0.2.0

- minor: Fixed workspace name in the README.md YAML fragments.

## 0.1.0

- minor: Initial release.

