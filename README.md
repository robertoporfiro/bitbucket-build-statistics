# Bitbucket Pipelines Pipe: Bitbucket build statistics

This pipe is used to get build minutes statistics at bitbucket repository level.

## YAML Definition

Add the following snippet to the script section of your `bitbucket-pipelines.yml` file:

```yaml
- pipe: atlassian/bitbucket-build-statistics:0.2.1
  variables:
    BITBUCKET_USERNAME: "<string>"
    BITBUCKET_APP_PASSWORD: "<string>"
    # WORKSPACE: "<string>" # Optional
    # REPO_LIST: "<string>" # Optional
    # FILENAME: "<string>" # Optional
    # BUILD_DAYS: "<number>" # Optional
    # DEBUG: "<boolean>" # Optional
```

## Variables

| Variable                     | Usage                                                                                  |
| ---------------------------- | ---------------------------------------------------------------------------------------|
| BITBUCKET_USERNAME (*)       | Bitbucket user that will be used for authentication.                                   |
| BITBUCKET_APP_PASSWORD  (*)  | [Bitbucket app password](https://support.atlassian.com/bitbucket-cloud/docs/app-passwords/) of the user who will be used   for authentication.                                                                                                     |
| WORKSPACE                    | Bitbucket workspace name. Default: `${BITBUCKET_WORKSPACE}`. Pipe will get details for all repositories from this workspace. Specify repository list using `REPO_LIST` variable if need to get details for only specific repositories.     |
| REPO_LIST                    | Bitbucket Repository list separated by space. The repository can be specified in the form of repo_name or account_name/repo_name. If no account name is specified for a repository then value for `WORKSPACE` variable will be used if mentioned else the workspace this Pipe is running will be used.    |
| FILENAME                     | File name to store Repository build minute details. The file will be created in `BITBUCKET_CLONE_DIR` directory. Default: `build_usage_date_{BITBUCKET_BUILD_NUMBER})`                                                                                                  
| BUILD_DAYS                   | Number of days to capture the build information. Default: 30                           | `build_usage_date_{BITBUCKET_BUILD_NUMBER})`                                                                                                  
| DEBUG                        | Turn on extra debug information. Default: `false`.                                     |

_(*) = required variable._

## Examples

### Basic example:

Get Pipeline details for all repositories in the workspace this Pipe is running.

```yaml
script:
  - pipe: atlassian/bitbucket-build-statistics:0.2.1
    variables:
      BITBUCKET_USERNAME: $BITBUCKET_USERNAME
      BITBUCKET_APP_PASSWORD: $BITBUCKET_APP_PASSWORD
```

### Advanced example:

Get Pipeline details for all repositories from the workspace specified in `WORKSPACE` variable.

```yaml
script:
  script:
    - pipe: atlassian/bitbucket-build-statistics:0.2.1
      variables:
        BITBUCKET_USERNAME: $BITBUCKET_USERNAME
        BITBUCKET_APP_PASSWORD: $BITBUCKET_APP_PASSWORD
        WORKSPACE: 'myworkspace'
```

Get Pipeline details for the repositories specified in `REPO_LIST` variable.

```yaml
script:
  script:
    - pipe: atlassian/bitbucket-build-statistics:0.2.1
      variables:
        BITBUCKET_USERNAME: $BITBUCKET_USERNAME
        BITBUCKET_APP_PASSWORD: $BITBUCKET_APP_PASSWORD
        REPO_LIST: 'my-awesome-repo myworkspace/the-best-repo'
```
Get Pipeline details for all repositories in the workspace this Pipe is running and store the result in build_usage.txt file.

```yaml
script:
  script:
    - pipe: atlassian/bitbucket-build-statistics:0.2.1
      variables:
        BITBUCKET_USERNAME: $BITBUCKET_USERNAME
        BITBUCKET_APP_PASSWORD: $BITBUCKET_APP_PASSWORD
        FILENAME: "build_usage.txt"
```
Get Pipeline details for last 10 days for all repositories in the workspace this Pipe is running.

```yaml
script:
  script:
    - pipe: atlassian/bitbucket-build-statistics:0.2.1
      variables:
        BITBUCKET_USERNAME: $BITBUCKET_USERNAME
        BITBUCKET_APP_PASSWORD: $BITBUCKET_APP_PASSWORD
        BUILD_DAYS: 10
```

## Support

If you’d like help with this pipe, or you have an issue or feature request, [let us know on Community](https://community.atlassian.com/t5/forums/postpage/board-id/bitbucket-pipelines-questions?add-tags=pipes,build,statistics).

If you’re reporting an issue, please include:

- the version of the pipe
- relevant logs and error messages
- steps to reproduce

## License
Copyright (c) 2020 Atlassian and others.
Apache 2.0 licensed, see [LICENSE.txt](LICENSE.txt) file.
