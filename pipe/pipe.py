import os
import datetime
from datetime import date
import dateutil.parser

import requests
from requests.auth import HTTPBasicAuth
from prettytable import PrettyTable

from bitbucket_pipes_toolkit import Pipe

today = date.today().strftime("%m_%d_%Y")

BITBUCKET_API_BASE_URL = "https://api.bitbucket.org/2.0"

schema = {
    'BITBUCKET_USERNAME': {'type': 'string', 'required': True},
    'BITBUCKET_APP_PASSWORD': {'type': 'string', 'required': True},
    'WORKSPACE': {'type': 'string', 'required': False, 'nullable': True, 'default': os.getenv('BITBUCKET_WORKSPACE')},
    'REPO_LIST': {'type': 'string', 'required': False, 'default': ""},
    'FILENAME': {'type': 'string', 'required': False,
                 'default': f"build_usage_{today}_{os.getenv('BITBUCKET_BUILD_NUMBER')}.txt"},
    'BUILD_DAYS': {'type': 'number', 'required': False, 'default': 30},
    'DEBUG': {'type': 'boolean', 'required': False, 'default': False},
}


class BuildStatisticsPipe(Pipe):
    def run(self):
        super().run()

        self.log_info("Executing the pipe...")

        self.bitbucket_username = self.get_variable('BITBUCKET_USERNAME')
        self.bitbucket_app_password = self.get_variable('BITBUCKET_APP_PASSWORD')
        self.bitbucket_workspace = self.get_variable('WORKSPACE')
        self.bitbucket_repo_list = self.get_variable('REPO_LIST')
        self.filename = self.get_variable('FILENAME')
        self.build_days = self.get_variable('BUILD_DAYS')
        self.debug = self.get_variable('DEBUG')

        self.log_info(f"Bitbucket workspace: {self.bitbucket_workspace}")
        self.log_info(f"Bitbucket repository list: {self.bitbucket_repo_list}")
        self.log_info(f"File Name: {self.filename}")
        self.log_info(f"Build days: {self.build_days}")
        self.log_info(f"Debug: {self.debug}")

        # Get the repository list based on the value specified in WORKSPACE and REPO_LIST
        self.get_repo_list()

        repo_pipeline_details = []
        # Get Pipeline details for the repositories
        for repo in self.bitbucket_repo_list:
            repo_pipeline_details.append(self.get_repo_pipeline_summary(repo))

        # Geneate the table
        prettyTable = PrettyTable()
        prettyTable.field_names = ["Repository", "Builds", "Build Duration"]
        prettyTable.align["Repository"] = "l"
        prettyTable.align["Builds"] = "c"
        prettyTable.align["Build Duration"] = "c"
        prettyTable.sortby = "Build Duration"
        prettyTable.reversesort = True

        for item in repo_pipeline_details:
            prettyTable.add_row([item["Repository"], item["Builds"], item["Build Duration"]])
        print(prettyTable.get_string(title="Bitbucket build statistics"))

        # Generate the file to capture output
        with open(self.filename, 'w') as f:
            f.write(prettyTable.get_string(title="Repository build details"))

        self.log_info(f"Successfully created file: {self.filename}")
        self.success(message="Success!")

    def get_repo_list(self):
        """
        Retrieves repository list
        :return: None
        """
        self.bitbucket_repo_list = self.bitbucket_repo_list.split()
        if not self.bitbucket_repo_list:
            # Request 100 repositories per page
            url = f"{BITBUCKET_API_BASE_URL}/repositories/{self.bitbucket_workspace}?pagelen=100&fields=next,values.full_name"
            auth = HTTPBasicAuth(self.bitbucket_username, self.bitbucket_app_password)

            # Keep fetching pages while there's a page to fetch
            while url is not None:
                try:
                    response = requests.get(url, auth=auth)
                    response.raise_for_status()
                except requests.exceptions.RequestException as e:
                    self.fail(f"{e}")
                response = response.json()
                for repo in response['values']:
                    reponame = repo['full_name']
                    self.bitbucket_repo_list.append(reponame)
                # Get the next page URL, if present
                url = response.get('next', None)
        else:
            for index in range(len(self.bitbucket_repo_list)):
                repo = self.bitbucket_repo_list[index]
                repo_full_name = repo if "/" in repo else f"{self.bitbucket_workspace}/{repo}"
                self.bitbucket_repo_list[index] = repo_full_name

    def get_repo_pipeline_summary(self, repo):
        """
        Retrieves pipelines summary of a repository
        :param repository_path: Repository path (ex: account/repo)
        :return: Pipeline summary for the repository
        """
        result = self.get_repo_pipelines(repo)
        now = datetime.datetime.now(datetime.timezone.utc)
        start_date = now - datetime.timedelta(self.build_days)
        build_seconds_used = 0
        build_count = 0
        for r in result:
            if dateutil.parser.parse(r['created_on']) > start_date:
                build_seconds_used += r['build_seconds_used']
                build_count += 1
        detail = {"Repository": repo, "Builds": build_count,
                  "Build Duration": self.convert_seconds_to_datetime(build_seconds_used)}
        return detail

    def convert_seconds_to_datetime(self, seconds):
        """
        Returns number of days, hours, minutes and seconds from the number of seconds
        :param seconds: number of seconds to convert
        :return: Coverted number of days, hours, minutes and seconds
        """
        days = seconds // (24 * 3600)
        seconds = seconds % (24 * 3600)
        hours = seconds // 3600
        seconds %= 3600
        minutes = seconds // 60
        seconds %= 60
        seconds = seconds

        return "{0}d {1}h {2}m {3}s".format(days, hours, minutes, seconds)

    def get_repo_pipelines(self, repo):
        """
        Retrieves pipelines list
        :param repository_path: Repository path (ex: account/repo)
        :return: List of pipelines information in JSON format
        """
        url = f"{BITBUCKET_API_BASE_URL}/repositories/{repo}/pipelines/"
        auth = HTTPBasicAuth(self.bitbucket_username, self.bitbucket_app_password)
        page = 1
        pipelines = []
        # Keep fetching pages while there's a page to fetch
        while True:
            try:
                params = {
                    'sort': "-created_on",
                    'page': page,
                    'pagelen': 100
                }
                response = requests.get(url, auth=auth, params=params)
                response.raise_for_status()
            except requests.exceptions.RequestException as e:
                self.fail(f"{e}")
            response = response.json()
            pipelines.extend(response['values'])
            if response['pagelen'] != 100:
                break
            page = page + 1
        return pipelines


if __name__ == '__main__':
    pipe = BuildStatisticsPipe(pipe_metadata_file='/pipe.yml', schema=schema, check_for_newer_version=True)
    pipe.run()
